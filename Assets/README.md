# Random Faster

* A simple and fast random number generator that can be substituted in place of System.Random, with extra methods and fast re-initialization.

## Installation

```bash
"com.yenmoc.random-faster":"https://gitlab.com/yenmoc/random-faster"
or
npm publish --registry=http://localhost:4873
```

## Method

```csharp
	-RandomFaster()
	-Next(int lowerBound, int upperBound)
	-RandomFaster(int seed)
	-Reinitialise(int seed)
	-int Next()
	-int Next(int upperBound)
	-int Next(int lowerBound, int upperBound)
	-double NextDouble()
	-NextBytes(byte[] buffer)
```

## Usages


```csharp

	ThreadSafeRandom.ThisThreadsRandom.Next(0, 1000000);

	var rndFaster = new RandomFaster(100000);
	var x = rndFaster.Next(0, 10000);
```

# Sample Benchmarks

```csharp
    private const int COUNT = 10000000;
    public void BrenchMark()
    {
        Profiler.BeginSample("random faster thread static");
        for (int i = 0; i < COUNT; i++)
        {
            var result = ThreadSafeRandom.ThisThreadsRandom.Next(1000);
        }
        Profiler.EndSample();
        
        
        Profiler.BeginSample("random faster");

        RandomFaster randomFaster = new RandomFaster(unchecked(System.Environment.TickCount * 31 + System.Threading.Thread.CurrentThread.ManagedThreadId));
        for (int i = 0; i < COUNT; i++)
        {
            var result = randomFaster.Next(1000);
        }

        Profiler.EndSample();

        
        Profiler.BeginSample("unity random");
        for (int i = 0; i < COUNT; i++)
        {
            var result = Random.Range(0, 1000);
        }

        Profiler.EndSample();

        
        Profiler.BeginSample("system random");

        System.Random rnd = new System.Random(500);
        for (int i = 0; i < COUNT; i++)
        {
            var result = rnd.Next(1000);
        }

        Profiler.EndSample();
    }
```

64bit Win 10, core i5 9400f coffe lake (6 Cores, 6 Threads)

# Next(int)

 |             Method 		| TEST_SIZE |     Mean       |     GC       |
 |--------------------------|-----------|----------------|--------------|
 | RandomFaster       		|  10000000 | 	 112.76 ms   |		40B		|
 | RandomFaster Thread safe |  10000000 | 	 148.86 ms   |		88B		|
 | UnityEngine.Random      	|  10000000 | 	 257.63 ms   |		48B		|
 | System.Random      		|  10000000 | 	 283.49 ms   |		288B	|


# Next(double)

 |             Method 		| TEST_SIZE |     Mean       |     GC       |
 |--------------------------|-----------|----------------|--------------|
 | RandomFaster       		|  10000000 | 	  96.15 ms   |		40B		|
 | RandomFaster Thread safe |  10000000 | 	 147.77 ms   |		88B		|
 | UnityEngine.Random      	|  10000000 | 	 198.65 ms   |		48B		|
 | System.Random      		|  10000000 | 	 280.06 ms   |		288B	|